﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BSS.View {
    /// <summary>
    /// View가 Open 후 일정 시간 후 자동으로 Close 시켜주는 컴포넌트
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(IView))]
    public class AutoClose : MonoBehaviour {
        public float autoCloseTime;
        private IView view;

        private void Start()
        {
            view = GetComponent<IView>();
            view.OnOpen += () => {
                StartCoroutine(CoAutoClose());
            };
        }

        /// <summary>
        /// 해당 View에 AutoClose 컴포넌트를 붙이고 적용합니다.
        /// </summary>
        public static AutoClose Apply(IView view, float time) {
            var autoClose=view.gameObject.AddComponent<AutoClose>();
            autoClose.autoCloseTime = time;
            return autoClose;
        }

        IEnumerator CoAutoClose() {
            yield return new WaitForSeconds(autoCloseTime);
            if (view!=null && view.isVisible) { view.Close(); }
        }
    }
}
