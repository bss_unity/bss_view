﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace BSS.View {
#if ODIN_INSPECTOR
    using Sirenix.OdinInspector;
#endif
    [DisallowMultipleComponent]
    public class ViewElement : MonoBehaviour {
        public static IEnumerable<ViewElement> runtimeElements => _runtimeElements;
        private static List<ViewElement> _runtimeElements = new List<ViewElement>();


        public ViewElement parent => transform.parent == null ? null : transform.parent.GetComponentInParent<ViewElement>();
#if ODIN_INSPECTOR
        [ShowInInspector]
#endif
        public string path => parent == null ? name : $"{parent.path}{SPLIT_CHARACTER}{name}";
        private readonly char SPLIT_CHARACTER = '/';

        public static T Search<T>() where T : ViewElement {
            return runtimeElements.FirstOrDefault(x => x is T) as T;
        }
        public static T Search<T>(string _path) where T : ViewElement {
            return runtimeElements.Where(x=>x is T).Select(x=>x as T).FirstOrDefault(x => x.path == _path);
        }


        public ViewElement Find(string _path) {
            _path = $"{path}{SPLIT_CHARACTER}{_path}";
            return GetComponentsInChildren<ViewElement>().FirstOrDefault(x => x.path == _path);
        }

        public T Find<T>(string _path) where T : ViewElement {
            _path = $"{path}{SPLIT_CHARACTER}{_path}";
            return GetComponentsInChildren<T>().FirstOrDefault(x => x.path == _path);
        }

        private void Awake() {
            _runtimeElements.Add(this);
        }
        private void OnDestroy() {
            _runtimeElements.Remove(this);
        }


    }

}
