﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BSS.View {
    public interface IView  {
        GameObject gameObject { get; }
        bool isVisible { get; }
        event Action OnOpen;
        event Action OnClose;

        void Open();
        void Close();

        void OpenInEditMode();
        void CloseInEditMode();
    }
}
