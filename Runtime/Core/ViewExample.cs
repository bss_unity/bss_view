﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
namespace BSS.View {
    public class CanvasGroupView : MonoBehaviour,IView
    {
        public bool isVisible => cg.alpha > 0.1f;
        private CanvasGroup cg => GetComponent<CanvasGroup>();

        public event Action OnOpen;
        public event Action OnClose;

        public void Open() {
            cg.alpha = 1f;
            OnOpen?.Invoke();
        }

        public void OpenInEditMode() {
            cg.alpha = 1f;
        }
        public void Close() {
            cg.alpha = 0f;
            OnClose?.Invoke();
        }

        public void CloseInEditMode() {
            cg.alpha = 0f;
        }
    }

    public class GameObjectActiveView : MonoBehaviour, IView {
        public bool isVisible => gameObject.activeInHierarchy;

        public event Action OnOpen;
        public event Action OnClose;

        public void Open() {
            gameObject.SetActive(true);
            OnOpen?.Invoke();
        }

        public void OpenInEditMode() {
            gameObject.SetActive(true);
        }
        public void Close() {
            OnClose?.Invoke();
            gameObject.SetActive(false);
        }

        public void CloseInEditMode() {
            gameObject.SetActive(false);
        }
    }
}
*/
